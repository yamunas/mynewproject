package utils;

import java.io.IOException;

import org.junit.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class Basicreport {
	
	ExtentHtmlReporter html; //to create a template
	ExtentReports extent; //create log
	ExtentTest test; //put test logs in report
	
	@Test
	
	public void runReport() {
	html = new ExtentHtmlReporter("./report/extentReport.html");
	extent = new ExtentReports();
	html.setAppendExisting(true); //existing and current run report append
	extent.attachReporter(html); 
	test = extent.createTest("TC001_Login","Login into Leadtaps");
	test.assignAuthor("Yamuna"); //who is going to execute the case
	test.assignCategory("Smoke");
	
	test.pass("This module ran sucessfully");
	try {
		test.fail("This module failed",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build()); //take screenshot of failed step
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	extent.flush(); //save in report
	
	}

}
