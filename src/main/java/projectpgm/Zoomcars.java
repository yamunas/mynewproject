package projectpgm;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class Zoomcars {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//a[@title='Start your wonderful journey']").click();
		driver.findElementByXPath("//div[text()='Popular Pick-up points']/following::div[2]").click();
		driver.findElementByClassName("proceed").click();
	    Date date1 = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		String today = sdf.format(date1);
		int tomorrow = Integer.parseInt(today)+1;		
		driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
		driver.findElementByClassName("proceed").click();
		driver.findElementByClassName("proceed").click();
		Thread.sleep(3000);
		List<WebElement> result = driver.findElementsByXPath("//div[@class='details']");
		System.out.println(result.size());
		List<WebElement> price = driver.findElementsByXPath("//div[@class='price']");
		List <String> ls = new LinkedList<>();
		
		for (WebElement webElement : price) {
			String number = webElement.getText();
			String replace = number.replaceAll("\\D", "");
			ls.add(replace);				
		}
		
		String max = Collections.max(ls);
		System.out.println(max);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("//div[contains(text(),'"+max+"')]/preceding::h3[1]")));
		Thread.sleep(3000);
		/*String carbrand = driver.findElementByXPath("(//div[@class='details'])[1]").getText();
		System.out.println(carbrand);
		driver.findElementByClassName("book-car").click();
		Thread.sleep(3000);
		driver.close();*/

	}

}
