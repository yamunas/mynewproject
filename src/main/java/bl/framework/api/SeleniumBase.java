package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import utils.AdvanceReport;

public class SeleniumBase extends AdvanceReport implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver(); 
			}
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The browser "+browser+" launched successfully");
			logStep("Browser Launched", "pass");
		} catch (WebDriverException e) {
			//System.err.println("Unknown Error" +e);
			logStep("Browser did not launch", "fail");
		}
        finally {
			takeSnap();
		}
        
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch (locatorType) {
			case "id": return driver.findElementById(value); // return web element
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "xpath": return driver.findElementByXPath(value);
			case "linktext" : return driver.findElementByLinkText(value);
			case "partiallink" : return driver.findElementByPartialLinkText(value);
			default:
				break;
			}
			logStep("Element is clicked", "pass");
			
		} catch (NoSuchElementException e) {
			logStep("Unknown Exception", "fail");
			
		}
		catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}
		return null; // since web element will throw exception so return null if no element is found
		
	}
	
	

	@Override
	public WebElement locateElement(String value) {
		return null;
	
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch (type) {
			case "xpath": return driver.findElementsByXPath(value);// return web element
			case "name": return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "id": return driver.findElementsById(value);
			case "linktext" : return driver.findElementsByLinkText(value);
			case "partiallink" : return driver.findElementsByPartialLinkText(value);
			case "tagname" : return driver.findElements(By.tagName(value));
			default:
				break;
			}
		} catch (WebDriverException e) {
		System.err.println("Unknown error");
		}
		finally {
		takeSnap();
		}
		return null;
	}
	
	@Override
	public void switchToAlert() {
		driver.switchTo().alert();

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
     Alert alert = driver.switchTo().alert();
     alert.getText();
		return null;
	}

	@Override
	public void typeAlert(String data) {
		driver.switchTo().alert().sendKeys(data);

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allwnd = driver.getWindowHandles();
			List<String> ls = new ArrayList<>(allwnd);
			driver.switchTo().window(ls.get(index));
			System.out.println("Window switched successfully");
		} catch (NoSuchWindowException e) {
			System.err.println("No such window found" +e);
		}
		catch (IndexOutOfBoundsException e) {
			System.err.println("Check the given number of windows" +e);
		}
		catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void switchToWindow(String title) {
		try {
			title = driver.getTitle();
			System.out.println(title);
		} catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
			System.out.println("switch to frames using index");
		} catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			System.out.println("switch to frames using web element");
		} catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
			System.out.println("Switch to frames using id/name");
		} catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			System.out.println("switched to default content");
		} catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png"); //folder created automatically
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		driver.close();

	}

	@Override
	public void quit() {
		driver.quit();

	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			logStep("Element is clicked", "pass");
			//System.out.println("The element "+ele+" clicked successfully");
		} catch (NoSuchElementException e) {
			System.err.println("Element has not found"  +e);
		}
		catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
		} catch (NoSuchElementException e) {
			System.err.println("Element has not found"  +e);
		}
		catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}
		
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data); 
			System.out.println("The data "+data+" entered successfully");
		} catch (NoSuchElementException e) {
			System.err.println("Element has not found"  +e);
		}
		catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select sel1 = new Select(ele);
			sel1.selectByVisibleText(value);
		} catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select sel1 = new Select(ele);
			sel1.selectByIndex(index);
		} catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select sel1 = new Select(ele);
			sel1.selectByValue(value);
		} catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public WebElement clickableNoSnap(WebElement ele) {
		try {
			WebDriverWait wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
		} catch (WebDriverException e) {
			System.err.println("Unknown Error"  +e);
		}
		return null;
	
	}
	
	public void sleep() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			System.err.println("Interrupted exeception");
		}
		
	}

}
