package bl.framework.testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.design.ProjectMethod;

public class TC004_DeleteLead extends ProjectMethod{

	@Test
	
	public void deletelead()
	
	{
	
	WebElement Createlead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
	Createlead.click();
	WebElement companyname = locateElement("id", "createLeadForm_companyName");
	clearAndType(companyname, "CitiCorp");
	WebElement lastname = locateElement("id", "createLeadForm_lastName");
	clearAndType(lastname, "Sivasankar");
	WebElement firstname = locateElement("id", "createLeadForm_firstName");
	clearAndType(firstname, "Yamuna");
    WebElement createleadbutton = locateElement("class", "smallSubmit");
    click(createleadbutton);
	WebElement FindLeads = locateElement("linktext", "Find Leads");
	FindLeads.click();
	WebElement findname = locateElement("xpath","(//input[@name='firstName'])[3]");
	clearAndType(findname, "Yamuna");
	WebElement clickfind = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
	click(clickfind);
	WebElement firstelement = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
	clickableNoSnap(firstelement);
	firstelement.click();
	WebElement delete = locateElement("linktext", "Delete");
	click(delete);
	sleep();
	quit();
}
}
