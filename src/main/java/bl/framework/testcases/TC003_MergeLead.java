package bl.framework.testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import bl.framework.design.ProjectMethod;

public class TC003_MergeLead extends ProjectMethod{
	
	
	@Test
	public void mergeLead()
	{
		
		WebElement Createlead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		Createlead.click();
		WebElement companyname = locateElement("id", "createLeadForm_companyName");
		clearAndType(companyname, "CitiCorp");
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		clearAndType(lastname, "Sivasankar");
		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstname, "Yamuna");
	    WebElement createleadbutton = locateElement("class", "smallSubmit");
	    click(createleadbutton);
	    WebElement createlead2 = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		createlead2.click();
		WebElement companyname2 = locateElement("id", "createLeadForm_companyName");
		clearAndType(companyname2, "BOA");
		WebElement lastname2 = locateElement("id", "createLeadForm_lastName");
		clearAndType(lastname2, "Kumaran");
		WebElement firstname2 = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstname2, "Balkies");
		WebElement createleadbutton2 = locateElement("class", "smallSubmit");
	    click(createleadbutton2);
		WebElement MergeLeads = locateElement("linktext", "Merge Leads");
		MergeLeads.click();
		WebElement fromlead = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		fromlead.click();
		switchToWindow(1);
		WebElement leadname = locateElement("xpath", "(//input[@class=' x-form-text x-form-field'])[2]");
	    clearAndType(leadname, "Yamuna");
	    WebElement findlead = locateElement("class", "x-btn-text");
	    findlead.click();
	    sleep();
	    WebElement clicklead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		clickableNoSnap(clicklead);
		clicklead.click();
		switchToWindow(0);
		WebElement tolead = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		tolead.click();
		switchToWindow(1);
		WebElement leadname2 = locateElement("xpath", "(//input[@class=' x-form-text x-form-field'])[2]");
	    clearAndType(leadname2, "Balkies");
	    WebElement findlead2 = locateElement("class", "x-btn-text");
	    findlead2.click();
	    sleep();
	    WebElement clicklead2 = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		clickableNoSnap(clicklead2);
		clicklead2.click();
		switchToWindow(0);
		WebElement mergbutton = locateElement("class", "buttonDangerous");
		mergbutton.click();
		acceptAlert();
		close();
		
	}
}