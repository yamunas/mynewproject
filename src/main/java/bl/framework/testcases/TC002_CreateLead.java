package bl.framework.testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.design.ProjectMethod;

public class TC002_CreateLead extends ProjectMethod{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "Yamuna";
		category = "Smoke";
	}
	
	@Test
	public void createlead()
	{
		
		WebElement Createlead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		Createlead.click();
		WebElement companyname = locateElement("id", "createLeadForm_companyName");
		clearAndType(companyname, "CitiCorp");
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		clearAndType(lastname, "Sivasankar");
		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstname, "Yamuna");
	    WebElement createleadbutton = locateElement("class", "smallSubmit");
	    click(createleadbutton);
	    
	}

}
