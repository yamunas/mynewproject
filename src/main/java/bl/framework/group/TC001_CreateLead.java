package bl.framework.group;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.design.ProjectMethod;

public class TC001_CreateLead extends ProjectMethod{
	
	@BeforeTest(groups = {"smoke"})
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "Gayatri";
		category = "Smoke";
	} 
	@Test(groups = {"smoke"})
	public void createLead() {
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), "TL");
		clearAndType(locateElement("id", "createLeadForm_firstName"), "koushik");
		clearAndType(locateElement("id", "createLeadForm_lastName"), "c");
		click(locateElement("name", "submitButton")); 
	}
}
	